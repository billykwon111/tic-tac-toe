import React from 'react';
import styled, { keyframes } from 'styled-components';
// @ts-ignore - no typings for this library
import slideInDown from 'react-animations/lib/rubber-band';

export const NavBar = styled.div`
	position: fixed;
	top: 0;
	width: 100%;
	background-color: #white;
	display: grid;
	grid-template-columns: 1fr repeat(3, auto) 1fr;
	grid-column-gap: 5px;
	justify-items: center;
	overflow: hidden;
	width: 100%;
`;

export const ScoreTracker = styled.h4`
	color: palegoldenrod;
	justify-content: center;
	padding: 14px;
	text-decoration: none;

	&:nth-child(1) {
		grid-column-start: 2;
	}
`;

export const ResetScores = styled.button`
	margin-left: auto;
	font-size: 20px;
	background-color: #14bdac;
	color: palegoldenrod;
	border-style: none;
	&:active {animation: 1.5s ${keyframes(slideInDown)};
`;

interface IHeader {
	scores: {
		X: number;
		O: number;
		D: number;
	};
	resetGame: (resetScore: boolean) => void;
}

const Header = (props: IHeader) => {
	return (
		<NavBar>
			<ScoreTracker>X Wins: {props.scores.X} </ScoreTracker>
			<ScoreTracker>O Wins: {props.scores.O} </ScoreTracker>
			<ScoreTracker>Draws: {props.scores.D} </ScoreTracker>
			<ResetScores onClick={() => props.resetGame(true)}>Reset Scores</ResetScores>
		</NavBar>
	);
};

export default Header;
