import React, { MouseEvent } from 'react';
import styled, { css, keyframes } from 'styled-components';
// @ts-ignore - no typings for this library
import rubberBand from 'react-animations/lib/rubber-band';

type ILetterProps = {
	shape: string | undefined;
};

const Letter = styled.div<ILetterProps>`
	animation: ${props => (props.shape ? css`1s ${keyframes(rubberBand)};` : '')};
`;

const TileDiv = styled.td`
	height: 33%;
	width: 33%;
    cursor: pointer;
    color: palegoldenrod;
	border: 10px solid #0da192;

	&:first-child {
		border-left-color: transparent;
		border-top-color: transparent;
	}

	&:last-child {
		border-right-color: transparent;
		border-top-color: transparent;
	}

	&:nth-of-type(2) {
		border-top-color: transparent;
	}

	&:nth-of-type(3) {
		border-right-color: transparent;
		border-top-color: transparent;
	}
`;

interface ITileProps {
	location: number;
	shape: string | undefined;
	handleTileClick(e: MouseEvent<HTMLElement>, location: number): void;
}

class Tile extends React.Component<ITileProps> {
	handleClick = (e: MouseEvent<HTMLElement>) => {
		const { location } = this.props;
		this.props.handleTileClick(e, location);
	};

	render() {
		return (
			<TileDiv onClick={this.handleClick}>
				<Letter shape={this.props.shape}>{this.props.shape}</Letter>
			</TileDiv>
		);
	}
}

export default Tile;
