import React, { MouseEvent } from 'react';
import styled from 'styled-components';
import Tile from './Tile';

const BoardDiv = styled.table`
	display: table;
	height: 300px;
	width: 300px;
	font-size: calc(12px + 2vmin);
	margin: 0 auto;

	@media (max-width: 650px) {
		height: 250px;
		width: 250px;
	}

	@media (max-height: 450px) {
		height: 150px;
		width: 150px;
	}
`;

const BoardRow = styled.tr`
	&:nth-of-type(3) {
		border-bottom-style: hidden;
	}
`;

interface IBoardProps {
	handleMove(location: number): void;
	tiles: string[];
}

class Board extends React.Component<IBoardProps> {
	handleTileClick = (event: MouseEvent<HTMLElement>, location: number) => {
		this.props.handleMove(location);
	};

	render() {
		const tiles = [];
		for (let i = 0; i < 9; i++) {
			tiles.push(
				<Tile key={i} location={i} shape={this.props.tiles[i]} handleTileClick={this.handleTileClick} />
			);
		}

		return (
			<BoardDiv>
				<tbody>
					<BoardRow>{tiles.slice(0, 3)}</BoardRow>
					<BoardRow>{tiles.slice(3, 6)}</BoardRow>
					<BoardRow>{tiles.slice(6, 9)}</BoardRow>
				</tbody>
			</BoardDiv>
		);
	}
}

export default Board;
