import React from 'react';
import styled, { keyframes } from 'styled-components';
import Board from './Board';
// @ts-ignore - no typings for this library
import slideInDown from 'react-animations/lib/slide-in-down';

const GameOverDiv = styled.div`
	border-radius: 20px;
	padding: 20px;
	background-color: MediumTurquoise;
	text-align: center;
	font-size: 2em;
	color: PeachPuff;
	display: block;
	animation: 1.5s ${keyframes(slideInDown)};
`;

const ResetButton = styled.button`
	height: 60;
	width: 150;
	font-size: 20px;
	font-weight: bold;
	border: none;
	border-radius: 10px;
	background-color: white;
	color: PeachPuff;
	text-align: center;
`;

const MessageDiv = styled.div`
	margin-top: 0.5em;
`;

interface IGameOver {
	winner: string;
	tiles: string[];
	resetGame: (resetScore: boolean) => void;
}

const GameOver = (props: IGameOver) => {
	const message = props.winner === 'D' ? "It's a tie!" : `Player ${props.winner} wins!`;

	return (
		<GameOverDiv>
			<MessageDiv>{message}</MessageDiv>
			<Board tiles={props.tiles} handleMove={() => {}}></Board>
			<ResetButton onClick={() => props.resetGame(false)}>Play Again</ResetButton>
		</GameOverDiv>
	);
};

export default GameOver;
