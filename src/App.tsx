import * as React from 'react';
import styled from 'styled-components';
import Header from './components/Header';
import Board from './components/Board';
import GameOver from './components/GameOver';
import './App.css';

/*
Board layout:
[ 0, 1, 2, 
  3, 4, 5,
  6, 7, 8 ]
*/

const GameDiv = styled.div`
	background-color: PowderBlue;
	text-align: center;
	font-family: 'Baloo Paaji';
	background-color: #14bdac;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	font-size: calc(10px + 2vmin);
  color: white;
`;

const TurnDiv = styled.div`
	margin-top: 2em;
`;

interface IScores {
	X: number;
	O: number;
	D: number;
}

interface IStateProps {
	totalMoves: number;
	scores: IScores;
	xMoves: number[];
	oMoves: number[];
	winner: string | null;
	tiles: string[];
}

class App extends React.Component {
	initialState: IStateProps;
	isXMove: boolean;
	maxMoves: number;

	state: IStateProps = {
		totalMoves: 0,
		scores: {
			X: 0,
			O: 0,
			D: 0,
		},
		xMoves: [],
		oMoves: [],
		winner: null,
		tiles: Array(9).fill(null),
	};

	constructor(props: IStateProps) {
		super(props);
		// Preserve initial state for reset
		this.initialState = this.state;
		this.isXMove = true;
		this.maxMoves = 9;
	}

	componentDidMount() {
		const localStorageRef = localStorage.getItem('scores');
		if (localStorageRef) {
			this.setState({
				scores: JSON.parse(localStorageRef),
			});
		}
	}

	handleMove = (location: number) => {
		// Only 9 moves allowed and cannot select already chosen tile
		if (this.state.totalMoves < this.maxMoves && !this.state.tiles[location] && !this.state.winner) {
			const tiles = [...this.state.tiles];
			const totalMoves = this.state.totalMoves + 1;

			if (this.isXMove) {
				tiles[location] = 'X';
				const xMoves = [...this.state.xMoves];
				xMoves.push(location);

				this.setState(
					{
						tiles,
						totalMoves,
						xMoves,
					},
					() => {
						this.checkWinner();
					}
				);
			} else {
				tiles[location] = 'O';
				const oMoves = [...this.state.oMoves];
				oMoves.push(location);

				this.setState(
					{
						tiles,
						totalMoves,
						oMoves,
					},
					() => {
						this.checkWinner();
					}
				);
			}

			this.isXMove = !this.isXMove;
		}
	};

	checkWinner = () => {
		// Check if draw
		if (this.state.totalMoves === this.maxMoves) {
			const winner = 'D';
			const scores = { ...this.state.scores };
			scores[winner] = scores[winner] + 1;

			this.setState({
				winner,
				scores,
			});
		} else if (this.state.totalMoves >= 5) {
			// Check winner based on the location of the move
			const winCombinations = [
				[0, 1, 2],
				[3, 4, 5],
				[6, 7, 8],
				[0, 3, 6],
				[1, 4, 7],
				[2, 5, 8],
				[0, 4, 8],
				[2, 4, 6],
			];

			// Slightly reversed logic since you need to check O moves when it is X's turn and vice versa
			const movesToCheck = this.isXMove ? this.state.oMoves : this.state.xMoves;

			for (const combo of winCombinations) {
				if (combo.every(move => movesToCheck.includes(move))) {
					const winner = this.isXMove ? 'O' : 'X';
					const scores = { ...this.state.scores };
					scores[winner] = scores[winner] + 1;

					this.setState({
						winner,
						scores,
					});
				}
			}
		}
	};

	resetGame = (resetScore: boolean) => {
		const scores = resetScore ? this.initialState.scores : { ...this.state.scores };

		localStorage.setItem('scores', JSON.stringify(scores));

		this.setState({
			...this.initialState,
			scores,
		});
	};

	render() {
		return (
			<GameDiv>
				<Header scores={this.state.scores} resetGame={this.resetGame} />

				{this.state.winner ? (
					<GameOver resetGame={this.resetGame} winner={this.state.winner} tiles={this.state.tiles} />
				) : (
					<React.Fragment>
						<Board handleMove={this.handleMove} tiles={this.state.tiles} />
						<TurnDiv>{this.isXMove ? 'X' : 'O'}'s Turn</TurnDiv>
					</React.Fragment>
				)}
			</GameDiv>
		);
	}
}

export default App;
